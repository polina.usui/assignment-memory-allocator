#define _DEFAULT_SOURCE
#include <unistd.h>
#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_SIZE 4196

#define MAP_FIXED 0x100000

static void* block_after( struct block_header const* block );

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd );

static void* map_pages(void const* addr, size_t length, int additional_flags);


void* heap_address;
struct block_header* block_h;

void test1() {
    printf("This is Test 1____________________\n");
    int* test = _malloc(1000);
    debug_heap(stdout, block_h);
    if (block_h -> is_free) {
        printf("Test 1 failed.\n");
        return;
    }
    debug_heap(stdout, heap_address);
    _free(test);
    printf("Test 1 passed.\n\n");
}

void test2() {
    printf("This is Test 2____________________\n");

    void *t1 = _malloc(100);
    void *t2 = _malloc(100);
    if (t1 == NULL || t2 == NULL) {
        printf("Test 2 failed. Can not allocate.\n");
    }

    debug_heap(stdout, block_h);
    _free(t1);
    debug_heap(stdout, block_h);

    struct block_header *t1_block = block_get_header(t1);
    struct block_header *t2_block = block_get_header(t2);

    if (t1_block -> is_free == false || t2_block -> is_free == true) {
        printf("Test 2 failed.\n\n");
        return;
    }
    _free(t2);
    printf("Test 2 passed.\n\n");
}

void test3() {
    printf("This is Test 3____________________\n");

    void* t1 = _malloc(900);
    void* t2 = _malloc(300);
    void* t3 = _malloc(30);

    debug_heap(stdout, block_h);
    _free(t2);
    _free(t1);
    debug_heap(stdout, block_h);

    struct block_header *t1_block = block_get_header(t1);
    struct block_header *t2_block = block_get_header(t2);
    struct block_header *t3_block = block_get_header(t3);

    if (t1_block -> is_free == false || t2_block -> is_free == false || t3_block -> is_free	 == true || 
        (t1_block->capacity.bytes != 900 + t2_block->capacity.bytes + offsetof(struct block_header, contents))) {
        printf("Test 3 failed.\n\n");
        return;
    }

    _free(t3);
    printf("Test 3 passed.\n\n");
}

void test4() {
    printf("This is Test 4____________________\n");
void* t1 = _malloc(4000);
void* t2 = _malloc(1000);
debug_heap(stdout, block_h);

if (t1 == NULL || t2 == NULL) {
    printf("Test 4 failed. _malloc returned null");
	return;
}
debug_heap(stdout, block_h);
struct block_header* h1 = block_get_header(t1);
struct block_header* h2 = block_get_header(t2);

if (!blocks_continuous(h1, h2))
{
    printf("Test 4 failed. Blocks are not adjacent.");
return;
}
debug_heap(stdout, block_h);
_free(t2);
_free(t1);

printf("Test 4 passed.\n\n");
}

void test5() {
    printf("This is Test 5____________________\n");
struct block_header* last_block = block_h;
map_pages(last_block, 1000, MAP_FIXED);
debug_heap(stdout, block_h);

void* data = _malloc(5 * 10000);
debug_heap(stdout, block_h);

if (blocks_continuous(last_block, block_get_header(data))) {
    printf("Test 5 failed. Blocks are adjacent.\n\n");
    return;
}
printf("Test 5 passed.\n\n");
}

bool test_init() {
    printf("Test initialization starts.\n");
    heap_address = heap_init(INITIAL_SIZE);
    if (heap_address == NULL) {
        printf("Initialization failed.\n");
        return false;
    }
    printf("Successful initialization.\n");
    return true;
}

int main() {
    if (test_init()) {
        block_h = (struct block_header*) heap_address;
        test1();
        test2();
        test3();
        test4();
        test5();
    } else return 1;

}

static void* block_after( struct block_header const* block ){
  return (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}
